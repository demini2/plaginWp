<?php

use admin\AdminController;
use admin\PaymentRepository;

$paymentRepository = new PaymentRepository();
$object = $paymentRepository->findById(1);

$apiKey = AdminController::API_KEY;
$apiCallUrl = AdminController::CALLBACK_URL;
$apiWebUrl = AdminController::WEBHOOK_URL;

if (!empty($object) && isset($object->api_key) && isset($object->callback_url) && isset($object->webhook_url)) {
    $apiKey = $object->api_key;
    $apiCallUrl = $object->callback_url;
    $apiWebUrl = $object->webhook_url;
}
?>
<form id="payment_admin_form" class="payment_admin_row">

    <label for="payment_admin_1">API_URL</label>
    <input id="payment_admin_1" value="<?= AdminController::API_URL ?>">

    <label for="payment_admin_2">API_KEY</label>
    <input id="payment_admin_2" value="<?= $apiKey ?>">

    <label for="payment_admin_3">CALLBACK_URL</label>
    <input id="payment_admin_3" value="<?= $apiCallUrl ?>">

    <label for="payment_admin_4">WEBHOOK_URL</label>
    <input id="payment_admin_4" value="<?= $apiWebUrl ?>">

    <button id="payment_admin_button" type="button" class="btn btn-outline-dark">Сохранить данные</button>
</form>
