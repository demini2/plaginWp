document.addEventListener('DOMContentLoaded', function () {
    let admin = document.getElementById('payment_admin_button');
    if (undefined !== admin && null !== admin) {
        admin.addEventListener('click', function (event) {
            event.preventDefault();

            let dataObj = {
                val1: document.getElementById('payment_admin_1').value,
                val2: document.getElementById('payment_admin_2').value,
                val3: document.getElementById('payment_admin_3').value,
                val4: document.getElementById('payment_admin_4').value,
            };

            jQuery.ajax({
                type: "POST",
                url: home_url + '/wp-admin/admin-ajax.php',
                data: {
                    action: 'form_payment_admin_action',
                    nonce: 'add_payment_admin_scripts_object',
                    data: dataObj
                },
                dataType: 'json',
                timeout: 30000,
                success: function (response) {
                    console.log('тут мы выводим сообщение об успешной оплате... или не успешной');
                    console.log(response);
                }
            });
        });
    }
});
