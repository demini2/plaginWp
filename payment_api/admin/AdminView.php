<?php

namespace admin;

/**
 * Клас отвечат за отображение админских вюшек
 */
class AdminView
{
    /**
     *  Статичный метод для отображения admin.php
     *
     * @return string
     */
    public static function getAdmin(): string
    {
        return require_once plugin_dir_path(__FILE__) . 'view/admin.php';
    }

    /**
     *  Статичный метод для отображения data.php
     *
     * @return string
     */
    public static function getData(): string
    {
        return require_once plugin_dir_path(__FILE__) . 'view/data.php';
    }
}
