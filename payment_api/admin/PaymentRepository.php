<?php

namespace admin;

use stdClass;

/**
 * Класс для работы с таблицей payment_data
 */
class PaymentRepository
{
    /**
     * Метод создания новой записи
     *
     * @param string $apiKey
     * @param string $callbackUrl
     * @param string $webhookUrl
     * @return bool|int
     */
    public function insert(string $apiKey, string $callbackUrl, string $webhookUrl): bool|int
    {
        global $wpdb;

        return $wpdb->insert(
            "wp_payment_data",
            [
                'api_key' => $apiKey,
                'callback_url' => $callbackUrl,
                'webhook_url' => $webhookUrl,
            ]
        );

    }

    /**
     * Обновление данных в таблице
     *
     * @param int $id
     * @param string $apiKey
     * @param string $callbackUrl
     * @param string $webhookUrl
     * @return bool|int
     */
    public function update(int $id, string $apiKey, string $callbackUrl, string $webhookUrl): bool|int
    {
        global $wpdb;

        return $wpdb->update(
            "wp_payment_data",
            [
                'api_key' => $apiKey,
                'callback_url' => $callbackUrl,
                'webhook_url' => $webhookUrl,
            ],
            [
                'id' => $id
            ]
        );
    }

    /**
     * Найти запись по id
     *
     * @param int $id
     * @return stdClass
     */
    public function findById(int $id): stdClass
    {
        global $wpdb;

        return $wpdb->get_results("SELECT * FROM `wp_payment_data` WHERE `id` = {$id}")[0];
    }
}
