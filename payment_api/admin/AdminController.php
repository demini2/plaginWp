<?php

namespace admin;

spl_autoload('PaymentRepository');

/**
 *  Контроллер админской части
 */
class AdminController
{
    const API_URL = 'https://api.fed.io/v1/';
    const API_KEY = 'jkhdsKJJbkjyiu8566GHG8tg*UU*Ghjuguiyf%^&f';

    const CALLBACK_URL = 'https://api.fed.io/v1/';

    const WEBHOOK_URL = 'https://api.fed.io/v1/';

    /**
     * Регистрируем скрипты css и js
     *
     * @return void
     */
    public function register(): void
    {
        wp_register_style('bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css', array(), '5.0.2');
        wp_enqueue_style('bootstrap-css');

        wp_register_style("payment_admin_style_front", plugins_url("payment_api/admin/css/payment_admin_style.css",), [], null);
        wp_register_script("payment_admin_script_front", plugins_url("payment_api/admin/js/payment_admin_scripts.js",), ['jquery'], null);
        wp_enqueue_style("payment_admin_style_front");
        wp_enqueue_script("payment_admin_script_front");

        add_action('wp_ajax_form_payment_admin_action', [$this, "actionAdmin"]);
        add_action('wp_ajax_nopriv_payment_admin_action', [$this, "actionAdmin"]);
    }

    /**
     * Тут пример запроса в db на создание новой или обновления данных для отправки
     *
     * @return void
     */
    public function actionAdmin(): void
    {
        $response = [0];
        if (isset($_POST['data']['val2']) && isset($_POST['data']['val3']) && isset($_POST['data']['val4'])) {

            $paymentRepository = new PaymentRepository();

            $object = $paymentRepository->findById(1);

            if (empty($object)) {
                $response = $paymentRepository->insert(
                    apiKey: (string)$_POST['data']['val2'],
                    callbackUrl: (string)$_POST['data']['val3'],
                    webhookUrl: (string)$_POST['data']['val4']
                );
            } else {
                $response = $paymentRepository->update(
                    id: $object->id,
                    apiKey: (string)$_POST['data']['val2'],
                    callbackUrl: (string)$_POST['data']['val3'],
                    webhookUrl: (string)$_POST['data']['val4']
                );
            }
        }

        wp_send_json($response);
    }
}
