<?php
/**
 * Plugin Name: Интеграция платежной системы
 * Description: Тестовый плагин интеграции платежной системы.
 * Version: 1.0.0
 * Author: Михаил Демченко
 * Author URI: https://t.me/demanys
 * Author email: demanys5@mail.ru
 * License: GPL2
 */

if (!defined('ABSPATH')) {
    die;
}

spl_autoload('admin/AdminView');
spl_autoload('admin/AdminController');
spl_autoload('client/ClientController');

use admin\AdminController;
use client\ClientController;
use admin\AdminView;

class PaymentApi
{
    /**
     * Регистрируем необходимые элементы для работы плагина
     *
     * @return void
     */
    public function register(): void
    {
        add_action('admin_menu', [$this, "addAdminMenu"]);
    }

    /**
     * Метод, вызывающийся при активации плагина
     *
     * @return void
     */
    public function activation(): void
    {
        $this->createTable();

        flush_rewrite_rules();
    }

    /**
     * Метод, вызывающийся при деактивации плагина
     *
     * @return void
     */
    public function deactivation(): void
    {
        $this->cleanTable();

        flush_rewrite_rules();
    }

    /**
     * Добавляем вкладку в боковое меню в админке
     *
     * @return void
     */
    function addAdminMenu(): void
    {
        add_menu_page(
            esc_html__('Платежная система', 'creator'),
            esc_html__('Платежная система', 'creator'),
            'manage_options',
            'payment',
            function () {
                AdminView::getAdmin();
            },

            'dashicons-category',
            100
        );
    }

    /**
     * Функция по очистке таблиц созданных плагином
     *
     * @return void
     */
    public function cleanTable(): void
    {
        global $wpdb;
        $prefix = $wpdb->get_blog_prefix();

        $table = $prefix . "payment_data";
        $wpdb->query($wpdb->prepare("TRUNCATE TABLE $table"));
    }

    /**
     * Функция создания таблиц для плагина
     *
     * @return void
     */
    private function createTable(): void
    {
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        global $wpdb;
        $charsetCollate = $wpdb->get_charset_collate();

        $prefix = $wpdb->get_blog_prefix();

        dbDelta($this->createPaymentData(prefix: $prefix, charsetCollate: $charsetCollate));
    }

    /**
     * Возвращает SQL запрос для создания таблицы с Данными
     *
     * @param string $prefix
     * @param string $charsetCollate
     * @return string
     */
    private function createPaymentData(string $prefix, string $charsetCollate): string
    {
        $tableName = $prefix . "payment_data";

        return "CREATE TABLE $tableName (
                id  bigint(7) unsigned NOT NULL auto_increment,
	            api_key text NOT NULL default '',
	            callback_url text NOT NULL default '',
	            webhook_url text NOT NULL default '',
	            PRIMARY KEY  (id)
	            )
	            $charsetCollate;";
    }
}

if (class_exists("PaymentApi")) {
    $paymentApi = new PaymentApi();
    $paymentApi->register();

    $clientController = new ClientController();
    $clientController->register();

    $adminController = new AdminController();
    $adminController->register();
}

register_activation_hook(__FILE__, [$paymentApi, "activation"]);
register_deactivation_hook(__FILE__, [$paymentApi, "deactivation"]);
