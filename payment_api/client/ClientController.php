<?php

namespace client;

/**
 *  Контроллер клиентской части
 */
class ClientController
{
    /**
     * Регистрируем скрипты css,js, шорткод
     *
     * @return void
     */
    public function register(): void
    {
        wp_register_style('bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css', array(), '5.0.2');
        wp_enqueue_style('bootstrap-css');

        wp_register_style("payment_client_style_front", plugins_url("payment_api/client/css/payment_client_style.css",), [], null);
        wp_enqueue_style("payment_client_style_front");

        wp_register_script("payment_client_script_front", plugins_url("payment_api/client/js/payment_client_scripts.js",), ['jquery'], null);
        wp_enqueue_script("payment_client_script_front");

        add_shortcode('payment_form', [$this, "title_page_payment"]);

        add_action('wp_ajax_form_payment_client_action', [$this, "actionClient"]);
        add_action('wp_ajax_nopriv_payment_client_action', [$this, "actionClient"]);
    }

    /**
     * Отображение шорткода
     *
     * @return bool|string
     */
    function title_page_payment(): bool|string
    {
        ob_start();
        ClientView::getClient();

        return ob_get_clean();
    }

    /**
     * Экшен обработки данных с клиента
     *
     * @return void
     */
    public function actionClient(): void
    {
        /** Тут мы делаем всю логику связанную с оплатой */
        $response = [
            'val1' => $_POST['data']['val1'],
            'val2' => $_POST['data']['val2'],
        ];
        wp_send_json($response);
    }
}
