document.addEventListener('DOMContentLoaded', function () {
    let client = document.getElementById('payment_client_button')
    if (undefined !== client && null !== client) {
        client.addEventListener('click', function (event) {
            event.preventDefault();

            let dataObj = {
                val1: document.getElementById('payment_1').value,
                val2: document.getElementById('payment_2').value,
            };

            jQuery.ajax({
                type: "POST",
                url: home_url + '/wp-admin/admin-ajax.php',
                data: {
                    action: 'form_payment_client_action',
                    nonce: 'add_payment_scripts_object',
                    data: dataObj
                },
                dataType: 'json',
                timeout: 30000,
                success: function (response) {
                    console.log('тут мы выводим сообщение об успешной оплате... или не успешной');
                    console.log(response.val1);
                    console.log(response.val2);
                }
            });
        });
    }
});
