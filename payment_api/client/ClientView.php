<?php

namespace client;

/**
 * Клас отвечат за отображение клиентских вюшек
 */
class ClientView
{
    /**
     *  Статичный метод для отображения client.php
     *
     * @return string
     */
    public static function getClient(): string
    {
        return require_once plugin_dir_path(__FILE__) . 'view/client.php';
    }

    /**
     * Статичный метод для отображения forma.php
     *
     * @return string
     */
    public static function getForm(): string
    {
        return require_once plugin_dir_path(__FILE__) . 'view/forma.php';
    }
}
